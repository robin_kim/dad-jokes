import React, { useState, useEffect } from 'react';
import { AiTwotoneHeart } from "react-icons/ai";
import { BiPlus } from "react-icons/bi";
import BlueBrick from "./assets/bluebrick.jpg";
import BlueLandscape from "./assets/bluelandscape.png";

function App() {
  const [joke, setJoke] = useState('');
  const [rating, setRating] = useState(null);
  const [message, setMessage] = useState('');

  useEffect(() => {
    fetchJoke();
  }, []);

  const fetchJoke = async () => {
    try {
      const response = await fetch('https://icanhazdadjoke.com/', {
        headers: {
          Accept: 'application/json',
        },
      });
      const data = await response.json();
      setJoke(data.joke);
      setRating(null);
      setMessage('');
    } catch (error) {
      console.log(error);
    }
  };

  const handleRating = (event) => {
    const rating = event.currentTarget.getAttribute('data-rating');
    let message = ``;
    if (rating < 3) {
      message = `That's unfortunate. We hope to crack a smile next time!`
    } else if (rating == 3) {
      message = `An average joke! It can be better..`
    } else if (rating > 3) {
      message = `That was hilarious! Quite the knee slapper!`
    }
    setRating(rating);
    setMessage(message);
    setJoke("")
    setTimeout(fetchJoke, 2000);
  };


  return (
    <div className="">
      <div
        className="text-center bg-cover bg-center bg-no-repeat w-full"
        style={{
          backgroundImage: `url(${BlueBrick})`,
          backgroundAttachment: "fixed",
        }}
      >
        <h1 className='bg-gray-900 bg-opacity-50 py-64 text-gray-50 font-bold text-5xl'>WELCOME TO DAD JOKE GENERATOR</h1>
      </div>
      <div className="bg-[#b6d6ff] py-20">
        <h2 className='text-center font-bold text-4xl underline underline-offset-4'>How to start:</h2>
        <p className='text-center font-semibold text-xl py-6'>
          Click the button below to generate a random dad joke. <br/>
          After the joke, give it a rating out of 5!
        </p>
      </div>
      <div
        className="text-center bg-cover bg-center bg-no-repeat w-full py-20"
        style={{
          backgroundImage: `url(${BlueLandscape})`,
          backgroundAttachment: "fixed",
          backgroundPosition: "22% 78%"
        }}>
        <div className='pt-8'>
          <h1 className="text-center font-bold text-4xl pt-6 pb-2 underline underline-offset-4">Generate Joke:</h1>
          <div className="flex display justify-center items-center pt-2 pb-8">
            <button onClick={fetchJoke} className='bg-blue-500 rounded-full w-8 h-8 flex justify-center items-center text-white hover:scale-110 ring-2'><BiPlus/></button>
          </div>
          <h2 className='text-xl text-center font-semibold px-20'>{joke}</h2>
        </div>
        {rating === null && (
          <div className="flex justify-center py-4">
            <button data-rating="1" className='px-1 text-3xl text-center hover:scale-125' onClick={handleRating}>
              <AiTwotoneHeart className='text-red-600' />
            </button>
            <button data-rating="2" className='px-1 text-3xl text-center hover:scale-125' onClick={handleRating}>
              <AiTwotoneHeart className='text-red-600' />
            </button>
            <button data-rating="3" className='px-1 text-3xl text-center hover:scale-125' onClick={handleRating}>
              <AiTwotoneHeart className='text-red-600' />
            </button>
            <button data-rating="4" className='px-1 text-3xl text-center hover:scale-125' onClick={handleRating}>
              <AiTwotoneHeart className='text-red-600' />
            </button>
            <button data-rating="5" className='px-1 text-3xl text-center hover:scale-125' onClick={handleRating}>
              <AiTwotoneHeart className='text-red-600' />
            </button>
          </div>
        )}
        {message && <div className="text-center text-xl font-semibold pb-20 text-black">{message}</div>}
      </div>
    </div>
  );
}

export default App;
